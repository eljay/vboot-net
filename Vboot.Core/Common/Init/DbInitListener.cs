﻿using System.Threading.Tasks;
using Furion.DependencyInjection;
using SqlSugar;
using Vboot.Core.Module.Sys;

namespace Vboot.Core.Common;

public class DbInitListener : ITransient
{
    private readonly SysOrgInit _orgInit;
    private readonly SysPortalInit _portalInit;
    private readonly ISqlSugarRepository<SysOrg> _sysOrgRepo;

    public DbInitListener(SysOrgInit orgInit,
        SysPortalInit portalInit,
        ISqlSugarRepository<SysOrg> sysOrgRepo)
    {
        _orgInit = orgInit;
        _portalInit = portalInit;
        _sysOrgRepo = sysOrgRepo;
    }

    public async Task Init()
    {
        var sysOrg= _sysOrgRepo.Single(it => it.id == "sa");
        if (sysOrg == null)
        {
            await _orgInit.InitAllDept();
            await _orgInit.InitAllUser();
            await _orgInit.InitZsf();
            await _orgInit.InitSa();
            await _portalInit.initPortal();
            await _portalInit.InitSysMenu();
            await _portalInit.InitSaMenu();
        }
    }

}