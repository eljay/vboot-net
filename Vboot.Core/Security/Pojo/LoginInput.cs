﻿namespace Vboot.Core.Security;

public class LoginInput
{
    /// <example>sa</example>
    public string username { get; set; }

    /// <example>1</example>
    public string password { get; set; }
}