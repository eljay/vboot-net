﻿using System.ComponentModel;
using SqlSugar;
using Vboot.Core.Common;

namespace Vboot.Core.Module.Ass;

[SugarTable("ass_dict_data", TableDescription = "字典值表")]
[Description("字典值表")]
public class AssDictData : BaseMainEntity
{
    [SugarColumn(ColumnDescription = "排序号", IsNullable = true)]
    public int ornum { get; set; }

    [SugarColumn(ColumnDescription = "字典ID", Length = 36)]
    public string maiid { get; set; }

    [SugarColumn(ColumnDescription = "代码", Length = 36)]
    public string code { get; set; }

    [SugarColumn(ColumnDescription = "备注", IsNullable = true, Length = 64)]
    public string notes { get; set; }
}