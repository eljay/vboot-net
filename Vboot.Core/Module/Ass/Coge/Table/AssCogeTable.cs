﻿using System.Collections.Generic;
using System.ComponentModel;
using SqlSugar;
using Vboot.Core.Common;

namespace Vboot.Core.Module.Ass;

[SugarTable("ass_coge_table", TableDescription = "代码生成-表信息")]
[Description("代码生成-表信息")]
public class AssCogeTable : BaseMainEntity
{
    [SugarColumn(ColumnDescription = "排序号", IsNullable = true)]
    public int ornum { get; set; }

    [SugarColumn(ColumnDescription = "备注", IsNullable = true, Length = 64)]
    public string notes { get; set; }

    [Navigate(NavigateType.OneToMany, nameof(AssCogeField.tabid))]
    [SugarColumn(IsIgnore = true)]
    public List<AssCogeField> fields { get; set; } = new List<AssCogeField>();

}