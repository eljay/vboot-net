﻿using System;
using System.IO;
using System.Threading.Tasks;
using Furion.DynamicApiController;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Vboot.Core.Common;
using Vboot.Core.Xutil;
using ZR.CodeGenerator;
using ZR.CodeGenerator.Model;

namespace Vboot.Core.Module.Ass;

[ApiDescriptionSettings("Ass", Tag = "字典配置")]
public class AssCogeTableApi : IDynamicApiController
{
    private readonly AssCogeTableService _service;
    
    private readonly IWebHostEnvironment _webHostEnvironment;
    
    private readonly IHttpContextAccessor _httpContextAccessor;

    public AssCogeTableApi(AssCogeTableService service,
        IWebHostEnvironment webHostEnvironment, 
        IHttpContextAccessor httpContextAccessor)
    {
        _service = service;
        _httpContextAccessor = httpContextAccessor;
        _webHostEnvironment = webHostEnvironment;
    }

    [QueryParameters]
    public async Task<dynamic> Get(string maiid, string name)
    {
        var pp = XreqUtil.GetPp();
        var items = await _service.repo.Context.Queryable<AssCogeTable>()
            .WhereIF(!string.IsNullOrWhiteSpace(name), t=> t.name.Contains(name.Trim()))
            .OrderBy(u => u.ornum)
            .Select((t) => new {t.id, t.name, t.crtim,t.uptim,t.notes})
            .ToPageListAsync(pp.page, pp.pageSize, pp.total);
        return RestPageResult.Build(pp.total.Value, items);
    }
    
    [QueryParameters]
    public async Task<dynamic> GetList(string maiid)
    {
        return await _service.repo.Context.Queryable<AssCogeTable>().ToListAsync();
    }
    
    [QueryParameters]
    public async Task<IActionResult> GetDownload(string maiid)
    {
        GenerateDto dto = new GenerateDto();
        AssCogeTable table= new AssCogeTable();
        table.name = "zzz";
        dto.GenTable = table;

        AssCogeField field = new AssCogeField();
        field.id = "11";
        field.name = "11";
        table.fields.Add(field);
        
        //自定义路径
        dto.ZipPath = Path.Combine(_webHostEnvironment.WebRootPath, "Generatecode");
        dto.GenCodePath = Path.Combine(dto.ZipPath, DateTime.Now.ToString("yyyyMMdd"));
        
        string zipReturnFileName = $"ZrAdmin.NET-{DateTime.Now:MMddHHmmss}.zip";
        
        //生成代码到指定文件夹
        CodeGeneratorTool.Generate(dto);
        FileHelper.ZipGenCode(dto.ZipPath, dto.GenCodePath, zipReturnFileName);
        Console.WriteLine("path="+dto.ZipPath);
        Console.WriteLine("filename="+zipReturnFileName);
        string zipFileFullName = Path.Combine(dto.ZipPath, zipReturnFileName);
        // Console.WriteLine("/Generatecode/"+zipReturnFileName);
        _httpContextAccessor.HttpContext.Response.Headers.Add("Content-Disposition", $"attachment; download-filename={zipReturnFileName}");
        // _httpContextAccessor.HttpContext.Response.Headers.Add("Access-Control-Expose-Headers", "Content-Disposition");
        _httpContextAccessor.HttpContext.Response.Headers.Add("Access-Control-Expose-Headers", "download-filename");
        _httpContextAccessor.HttpContext.Response.Headers.Add("download-filename", zipReturnFileName);
        return new FileStreamResult(new FileStream(zipFileFullName, FileMode.Open), "application/octet-stream") { FileDownloadName = zipReturnFileName };
        //下载文件
    }

    public async Task<AssCogeTable> GetOne(string id)
    {
        var data = await _service.repo.Context.Queryable<AssCogeTable>()
            .Where(it => it.id == id).FirstAsync();
        return data;
    }

    public async Task Post(AssCogeTable data)
    {
        await _service.InsertAsync(data);
    }

    public async Task Put(AssCogeTable data)
    {
        await _service.UpdateAsync(data);
    }

    public async Task Delete(string ids)
    {
        await _service.DeleteAsync(ids);
    }
}