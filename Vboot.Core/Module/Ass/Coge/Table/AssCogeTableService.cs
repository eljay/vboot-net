﻿using Furion.DependencyInjection;
using SqlSugar;
using Vboot.Core.Common;

namespace Vboot.Core.Module.Ass;

public class AssCogeTableService : BaseMainService<AssCogeTable>, ITransient
{
    public AssCogeTableService(ISqlSugarRepository<AssCogeTable> repo)
    {
        base.repo = repo;
    }
}