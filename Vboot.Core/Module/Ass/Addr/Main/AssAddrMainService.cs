﻿using Furion.DependencyInjection;
using SqlSugar;
using Vboot.Core.Common;
using Vboot.Core.Module.Ass;

namespace Vboot.Core.Module.Ass;

public class AssAddrMainService : BaseMainService<AssAddrMain>, ITransient
{
    public AssAddrMainService(ISqlSugarRepository<AssAddrMain> repo)
    {
        this.repo = repo;
    }
}