﻿using System.Threading.Tasks;
using Furion.DynamicApiController;
using Microsoft.AspNetCore.Mvc;
using Vboot.Core.Common;
using Vboot.Core.Xutil;

namespace Vboot.Core.Module.Ass;

[ApiDescriptionSettings("Ass", Tag = "省市区县")]
public class AssAddrMainApi : IDynamicApiController
{
    private readonly AssAddrMainService _service;

    public AssAddrMainApi(AssAddrMainService service)
    {
        _service = service;
    }
    
    public async Task<dynamic> GetList()
    {
        return await _service.repo.ToListAsync();
    }

    [QueryParameters]
    public async Task<dynamic> Get( string name)
    {
        var pp = XreqUtil.GetPp();
        var items = await _service.repo.Context.Queryable<AssAddrMain>()
            .WhereIF(!string.IsNullOrWhiteSpace(name), t 
                => t.name.Contains(name.Trim())||t.id.Contains(name.Trim()))
            .ToPageListAsync(pp.page, pp.pageSize, pp.total);
        return RestPageResult.Build(pp.total.Value, items);
    }

    public async Task<AssAddrMain> GetOne(string id)
    {
        return await _service.SingleAsync(id);
    }

    public async Task Post(AssAddrMain main)
    {
        await _service.InsertAsync(main);
    }

    public async Task Put(AssAddrMain main)
    {
        await _service.UpdateAsync(main);
    }

    public async Task Delete(string ids)
    {
        await _service.DeleteAsync(ids);
    }
}