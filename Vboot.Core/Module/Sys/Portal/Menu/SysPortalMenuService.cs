﻿using Furion.DependencyInjection;
using SqlSugar;
using Vboot.Core.Common;

namespace Vboot.Core.Module.Sys;

public class SysPortalMenuService : BaseMainService<SysPortalMenu>, ITransient
{
    public SysPortalMenuService(ISqlSugarRepository<SysPortalMenu> repo)
    {
        base.repo = repo;
    }
}