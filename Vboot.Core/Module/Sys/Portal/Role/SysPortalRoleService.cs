﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Furion.DependencyInjection;
using SqlSugar;
using Vboot.Core.Common;

namespace Vboot.Core.Module.Sys;

public class SysPortalRoleService : BaseMainService<SysPortalRole>, ITransient
{
    public SysPortalRoleService(ISqlSugarRepository<SysPortalRole> repo)
    {
        base.repo = repo;
    }

    public async Task InsertAsync(SysPortalRole role, List<SysPortalRoleToOrg> romaps, List<SysPortalRoleToMenu> rmmaps)
    {
        using var tran = repo.Context.UseTran();
        await base.InsertAsync(role);
        await repo.Context.Insertable(romaps).ExecuteCommandAsync();
        await repo.Context.Insertable(rmmaps).ExecuteCommandAsync();
        tran.CommitTran();
    }

    public async Task UpdateAsync(SysPortalRole role, List<SysPortalRoleToOrg> romaps, List<SysPortalRoleToMenu> rmmaps)
    {
        using var tran = repo.Context.UseTran();
        await base.UpdateAsync(role);
        await repo.Context.Deleteable<SysPortalRoleToOrg>().Where(it => it.rid == role.id).ExecuteCommandAsync();
        await repo.Context.Insertable(romaps).ExecuteCommandAsync();
        await repo.Context.Deleteable<SysPortalRoleToMenu>().Where(it => it.rid == role.id).ExecuteCommandAsync();
        await repo.Context.Insertable(rmmaps).ExecuteCommandAsync();
        tran.CommitTran();
    }
}