﻿using System.Collections.Generic;
using SqlSugar;
using Vboot.Core.Common;
using Vboot.Core.Module.Sys;

namespace Vboot.Core.Module.Sys;

[SugarTable("sys_portal_main", TableDescription = "门户主信息")]
public class SysPortalMain : BaseMainEntity
{
    [SugarColumn(ColumnDescription = "排序号", IsNullable = true)]
    public int ornum { get; set; }

    [SugarColumn(ColumnDescription = "备注", IsNullable = true, Length = 64)]
    public string notes { get; set; }
    
}