﻿using Furion.DependencyInjection;
using SqlSugar;
using Vboot.Core.Common;

namespace Vboot.Core.Module.Sys;

public class SysPortalMainService : BaseMainService<SysPortalMain>, ITransient
{
    public SysPortalMainService(ISqlSugarRepository<SysPortalMain> repo)
    {
        this.repo = repo;
    }
}