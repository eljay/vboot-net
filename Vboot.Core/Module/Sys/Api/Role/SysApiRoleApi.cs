﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Furion.DynamicApiController;
using Microsoft.AspNetCore.Mvc;
using SqlSugar;
using Vboot.Core.Common;
using Vboot.Core.Xutil;
using Yitter.IdGenerator;

namespace Vboot.Core.Module.Sys;

[ApiDescriptionSettings("Sys", Tag = "接口管理-角色")]
public class SysApiRoleApi : IDynamicApiController
{
    private readonly SysApiRoleService _service;

    public SysApiRoleApi(
        SysApiRoleService service)
    {
        _service = service;
    }

    [QueryParameters]
    public async Task<dynamic> Get(string name)
    {
        var pp = XreqUtil.GetPp();
        var items = await _service.repo.Context.Queryable<SysApiRole>()
            .WhereIF(!string.IsNullOrWhiteSpace(name), t => t.name.Contains(name.Trim()))
            .OrderBy(u => u.ornum)
            .Select((t) => new {t.id, t.name, t.notes, t.crtim, t.uptim})
            .ToPageListAsync(pp.page, pp.pageSize, pp.total);
        return RestPageResult.Build(pp.total.Value, items);
    }

    public async Task<SysApiRole> GetOne(string id)
    {
        var role = await _service.repo.Context.Queryable<SysApiRole>()
            .Mapper<SysApiRole, SysOrg, SysApiRoleToOrg>(it =>
                ManyToMany.Config(it.rid, it.oid))
            .Mapper<SysApiRole, SysApiMain, SysApiRoleToApi>(it =>
                ManyToMany.Config(it.rid, it.mid))
            .Where(it => it.id == id).FirstAsync();
        return role;
    }

    public async Task Post(SysApiRole role)
    {
        role.id = YitIdHelper.NextId() + "";
        var romaps = new List<SysApiRoleToOrg>();
        foreach (var org in role.orgs)
        {
            romaps.Add(new SysApiRoleToOrg {rid = role.id, oid = org.id});
        }

        var rmmaps = new List<SysApiRoleToApi>();
        foreach (var api in role.apis)
        {
            rmmaps.Add(new SysApiRoleToApi {rid = role.id, mid = api.id});
        }

        await _service.InsertAsync(role, romaps, rmmaps);
    }

    public async Task PostFlush()
    {
        await _service.repo.Context.Updateable<SysOrgUser>().SetColumns(it => it.retag == false)
            .Where(it => it.retag == true)
            .ExecuteCommandAsync();
    }

    public async Task Put(SysApiRole role)
    {
        var romaps = new List<SysApiRoleToOrg>();
        foreach (var org in role.orgs)
        {
            romaps.Add(new SysApiRoleToOrg {rid = role.id, oid = org.id});
        }

        var rmmaps = new List<SysApiRoleToApi>();
        foreach (var api in role.apis)
        {
            rmmaps.Add(new SysApiRoleToApi {rid = role.id, mid = api.id});
        }

        await _service.UpdateAsync(role, romaps, rmmaps);
    }

    public async Task Delete(string ids)
    {
        var idArr = ids.Split(",");
        await _service.DeleteAsync(ids);
    }
}