﻿using SqlSugar;

namespace Vboot.Core.Module.Sys;

[SugarTable("sys_api_role_api", TableDescription = "接口角色API对应表")]
public class SysApiRoleToApi
{
    [SugarColumn(ColumnDescription = "角色ID", IsNullable = true)]
    public string rid { get; set; }

    [SugarColumn(ColumnDescription = "菜单ID", IsNullable = true)]
    public string mid { get; set; }
}