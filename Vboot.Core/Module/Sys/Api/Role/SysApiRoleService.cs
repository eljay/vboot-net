﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Furion.DependencyInjection;
using SqlSugar;
using Vboot.Core.Common;

namespace Vboot.Core.Module.Sys;

public class SysApiRoleService : BaseMainService<SysApiRole>, ITransient
{
    public SysApiRoleService(ISqlSugarRepository<SysApiRole> repo)
    {
        base.repo = repo;
    }

    public async Task InsertAsync(SysApiRole role, List<SysApiRoleToOrg> romaps, List<SysApiRoleToApi> rmmaps)
    {
        using var tran = repo.Context.UseTran();
        await base.InsertAsync(role);
        await repo.Context.Insertable(romaps).ExecuteCommandAsync();
        await repo.Context.Insertable(rmmaps).ExecuteCommandAsync();
        tran.CommitTran();
    }

    public async Task UpdateAsync(SysApiRole role, List<SysApiRoleToOrg> romaps, List<SysApiRoleToApi> rmmaps)
    {
        using var tran = repo.Context.UseTran();
        await base.UpdateAsync(role);
        await repo.Context.Deleteable<SysApiRoleToOrg>().Where(it => it.rid == role.id).ExecuteCommandAsync();
        await repo.Context.Insertable(romaps).ExecuteCommandAsync();
        await repo.Context.Deleteable<SysApiRoleToApi>().Where(it => it.rid == role.id).ExecuteCommandAsync();
        await repo.Context.Insertable(rmmaps).ExecuteCommandAsync();
        tran.CommitTran();
    }
}