﻿
using System;
using Vboot.Web.Core;

namespace Vboot.Core.Module.Sys;

public class SysApiMainCache
{
    public static Yapi[] GET_URLS = Array.Empty<Yapi>();

    public static Yapi[] POST_URLS = Array.Empty<Yapi>();

    public static Yapi[] PUT_URLS = Array.Empty<Yapi>();

    public static Yapi[] DELETE_URLS = Array.Empty<Yapi>();

    public static int AUTHPOS = 0;
}