﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using SqlSugar;

namespace Vboot.Core.Module.Sys;

[SugarTable("sys_api_main", TableDescription = "API接口")]
[Description("API接口")]
public class SysApiMain
{
    [SugarColumn(ColumnDescription = "Id主键", IsPrimaryKey = true, Length = 64)]
    public string id { get; set; }

    [SugarColumn(ColumnDescription = "上级接口", IsNullable = true, Length = 64)]
    public string pid { get; set; }
    
    [SugarColumn(ColumnDescription = "接口名称", IsNullable = true, Length = 64)]
    public string name { get; set; }
    
    [SugarColumn(IsIgnore = true)]
    public List<SysApiMain> children { get; set; }

    [SugarColumn(ColumnDescription = "接口url", IsNullable = true, Length = 128)]
    public string url { get; set; }

    [SugarColumn(ColumnDescription = "权限代码", IsNullable = true, IsOnlyIgnoreUpdate = true)]
    public long code { get; set; }

    [SugarColumn(ColumnDescription = "权限位", IsNullable = true, IsOnlyIgnoreUpdate = true)]
    public int pos { get; set; }

    [SugarColumn(ColumnDescription = "接口类型", IsNullable = true)]
    public string type { get; set; }

    [SugarColumn(ColumnDescription = "是否可用", IsNullable = true)]
    public bool avtag { get; set; } = true;
    
    
    [SugarColumn(ColumnDescription = "创建时间", IsNullable = true, IsOnlyIgnoreUpdate = true)]
    public DateTime? crtim { get; set; } = DateTime.Now;

    [SugarColumn(ColumnDescription = "更新时间", IsNullable = true)]
    public DateTime? uptim { get; set; }
}