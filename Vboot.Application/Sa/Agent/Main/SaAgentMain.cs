﻿using System.Collections.Generic;
using SqlSugar;
using Vboot.Core.Common;
using Vboot.Core.Module.Sys;

namespace Vboot.Application.Sa;

[SugarTable("sa_agent_main", TableDescription = "代理商主信息")]
public class SaAgentMain : BaseMainEntity
{
    /// <summary>
    /// 完整地址
    /// </summary>
    [SugarColumn(ColumnDescription = "完整地址", IsNullable = true, Length = 255)]
    public string addre { get; set; }
    
    /// <summary>
    /// 省市区
    /// </summary>
    [SugarColumn(ColumnDescription = "省市区", IsNullable = true, Length = 64)]
    public string adreg { get; set; }
    
    /// <summary>
    /// 省市区以外的详细信息
    /// </summary> 
    [SugarColumn(ColumnDescription = "省市区以外的详细信息", IsNullable = true, Length = 128)]
    public string addet { get; set; }
    
    /// <summary>
    /// 经纬度
    /// </summary> 
    [SugarColumn(ColumnDescription = "经纬度", IsNullable = true, Length = 32)]
    public string adcoo { get; set; }
    
    /// <summary>
    /// 省
    /// </summary> 
    [SugarColumn(ColumnDescription = "省", IsNullable = true, Length = 32)]
    public string adpro { get; set; }
    
    /// <summary>
    /// 市
    /// </summary> 
    [SugarColumn(ColumnDescription = "市", IsNullable = true, Length = 32)]
    public string adcit { get; set; }
    
    /// <summary>
    /// 区
    /// </summary> 
    [SugarColumn(ColumnDescription = "区", IsNullable = true, Length = 32)]
    public string addis { get; set; }

    /// <summary>
    /// 代理商资质
    /// </summary> 
    [SugarColumn(ColumnDescription = "代理商资质", IsNullable = true, Length = 32)]
    public string level { get; set; }
    
    [SugarColumn(ColumnDescription = "流水号", IsNullable = true, Length = 32)]
    public string senum { get; set; }
    
    /// <summary>
    /// 经办人ID
    /// </summary>
    [SugarColumn(ColumnName = "opman",ColumnDescription = "经办人ID", IsNullable = true, Length = 36)]
    public string opmid { get; set; }
    
    /// <summary>
    /// 经办人
    /// </summary>
    [SugarColumn(IsIgnore = true)]
    public SysOrg opman { get; set; }

    /// <summary>
    /// 可查看者
    /// </summary>
    [SugarColumn(IsIgnore = true)]
    public List<SysOrg> viewers { get; set; } = new ();
    

}